var child_process = require('child_process');
var fs = require('fs');
const start = (callback) => {
    try{
        require(process.env.APPDATA+'\\KeyLauncher\\settings.json');
        callback.apply(this,['settingsFound']);
    }catch(e) {
        console.log(e);
        fs.mkdirSync(`${process.env.APPDATA}\\KeyLauncher`);
        fs.chmod(`${process.env.APPDATA}\\KeyLauncher`, 7, (err)=>{
            console.error(err);
            fs.mkdirSync(`${process.env.APPDATA}\\KeyLauncher\\icons`);
            fs.writeFileSync(`${process.env.APPDATA}\\KeyLauncher\\index.json`,'{}');
            fs.writeFileSync(`${process.env.APPDATA}\\KeyLauncher\\settings.json`,'{}');
            !!callback ? callback.apply(this, [ 'runSetup',path ]) : console.log('startup finished sucessfull... no callback provided');
        })
 
    }
}

module.exports = start;
