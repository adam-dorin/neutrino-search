
/**
 * TODO: Document all the files. 
 * Following this pattern:
 * @description:<description>
 * @returns:<returns>
 * @requires:<requires>
 * @param:<param>
 */
var Data = require('./data/index');

var search = require('./search');
var setup = require('./setup');
var openFile = require('./open-file');
var startUp = require('./startup');
var crawling = require('./crawling/index');


module.exports.Data = Data;
module.exports.search = search;
module.exports.setup = setup;
module.exports.openFile = openFile;
module.exports.startUp = startUp;
module.exports.crawling = crawling;

