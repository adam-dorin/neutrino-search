var fs = require('fs');
var child_process = require('child_process');
var startUp = require('../startup');
var setup = require('../setup');
var most_used = require('../crawling/most-used-location');

Array.prototype.unique =  // extend the array prototype
    [].unique ||              // if it doesn't exist
    function (
        a
    ) {
        return function () {     // with a function that
            return this.filter(a) // filters by the cached function
        }
    }(
        function (a, b, c) {       // which
            return c.indexOf(     // finds out whether the array contains
                a,                  // the item
                b + 1               // after the current index
            ) < 0                 // and returns false if it does.
        }
    );

var BASE_PATH = process.env.APPDATA + '\\KeyLauncher';//child_process.execFileSync('back-end\\data\\path.bat').toString().trim();
var APP_DATA = process.env.APPDATA;//child_process.execFileSync('back-end\\data\\appData.bat').toString().trim();
var RESOURCES = {};
try {

    RESOURCES = {
        INDEX: require(`${BASE_PATH}\\index.json`),
        SETTINGS: require(`${BASE_PATH}\\settings.json`)
    };
} catch (e) {
    RESOURCES = {
        INDEX: require(`${__dirname}\\base.json`).index,
        SETTINGS: require(`${__dirname}\\base.json`).settings
    };
    RESOURCES.SETTINGS.paths.most_used_locations[0].root = process.env.APPDATA + RESOURCES.SETTINGS.paths.most_used_locations[0].root;

}

var Resources = function () {
    var context = {};
    this.setup = () => {
        // BASE_PATH = path;
        RESOURCES = {
            INDEX: require(`${BASE_PATH}\\index.json`),
            SETTINGS: require(`${BASE_PATH}\\settings.json`)
        };
    };
    this.get = (type) => {
        try {

            return require(`${BASE_PATH}\\${type.toLowerCase()}.json`);
        } catch (e) {
            return RESOURCES[type.toUpperCase()];
        }
    };
    this.set = (type, key, data) => {
        // console.log(type,key,data);
        if(!type || !data){
            throw 'type and data are mandatory ->Resources@set';
        }
        if (!!key) {
            let itemsToPush = data.filter(item => {
                return RESOURCES[type.toUpperCase()][key].filter(pushedItem => {
                    return pushedItem.path === item.path;
                }).length === 0;
            })
            RESOURCES[type.toUpperCase()][key] = [...RESOURCES[type.toUpperCase()][key], ...itemsToPush].unique();
        }
        if(!key){
            RESOURCES[type.toUpperCase()] = data;
        }
        this.sync(type);
    }
    this.path = () => {
        return BASE_PATH;
    }
    this.appData = () => {
        return APP_DATA;
    }
    this.sync = (type, callback) => {
        fs.writeFile(`${BASE_PATH}\\${type.toLowerCase()}.json`, JSON.stringify(RESOURCES[type.toUpperCase()]), (err) => {
            !!callback ? callback.apply(this, []) : false;
        })
    }
}

var instance = new Resources();
setInterval(() => {
    instance.sync('index', () => {
        console.log('index updated!');
        console.log('verify', require(`${BASE_PATH}\\index.json`));
        instance.sync('settings', () => {
            console.log('settings updated!');
            console.log('verify', require(`${BASE_PATH}\\index.json`));
        })
    })
}, 1200000)

module.exports = new Resources();

