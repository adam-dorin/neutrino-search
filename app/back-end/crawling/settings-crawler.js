

var fs = require('fs');
var child_process = require('child_process');

var Data = require('../data/index');
var logger = require('../../utilities').logger(true);



var IndexModel = function (name, description, path, icon) {

    this.name = name || "";
    this.description = description || "";
    this.path = path || "";
    this.icon = icon || "";
}
var start = () => {
    
    var SETTINGS = Data.get('settings');
    var INDEX = Data.get('index');

    var indexPaths = [...SETTINGS.paths.index];
    var numberOfPaths = indexPaths.length;
    INDEX.data = [];
    var children = {};
    var valid_records = 0;


    logger(SETTINGS);
    logger('Starting to index...');

    logger('Emptied INDEX');
    logger(`There are ${indexPaths.length} paths to index`);

    indexPaths.map(path => {
        logger('INDEXING: ', path, __dirname);
        // add new param .... the exclude
        children[path.root] = child_process.spawn(`node`, [`${__dirname}\\crawler.js`, `${path.root}`, `${path.fileFilter}`]);

        children[path.root].stdout.on('data', (data) => {

            if (data.toString().indexOf('info: ') !== -1) {
                logger(data.toString());
            } else {

                // console.log('about to push',data.toString().split('\n') );

                var input = data.toString().split('\n');
                input.map(instance => {
                    try {
                        let dat = JSON.parse(instance);
                        !!dat.name && !!dat.path ? valid_records += 1 : null;
                        INDEX.data.push(new IndexModel(dat.name, '', dat.path, ''));
                    } catch (error) {
                        // console.log(instance);
                    }
                });
                INDEX.data.length % 100 === 0 ? logger(`${INDEX.data.length} indexed so far...`) : null;
            }
        });

        children[path.root].stderr.on('data', (data) => {
            logger(`child stderr:\n${data}`);
        });

        children[path.root].on('exit', function (code, signal) {
            numberOfPaths--;
            logger(`Crawling ${path.root} ended with
        code ${code}
        signal ${signal}
        paths remain: ${numberOfPaths}
        valid records: ${valid_records}
        total records: ${INDEX.data.length}`);

            if (!numberOfPaths) {
                fs.writeFile('./data/index.json', JSON.stringify(INDEX), 'utf8', (err, data) => {
                    if (err) {
                        logger(err);
                        process.exit();
                    }
                    logger('Index Saved!');
                    process.exit();
                });
            }
        });

    });
}

module.exports = start;