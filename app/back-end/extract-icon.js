const start = (message,callback) => {
    var iconExtractor = require('icon-extractor');
    var base64Img = require('base64-img');
    var util = require('../utilities');
    var ws = require('windows-shortcuts');

    var Data = require('./data/index');

    var _setResults = (iconData) => {
        // Some sort of memory leak is still here but @least it doest write 10k icons on startup
        // TODO: Find a way to limit the numbers of the calls.
        base64Img.img(`data:image/png;base64,${iconData.Base64ImageData}`,
            `${Data.path()}\\icons`,
            `${iconData.Context.split('\\').pop()}`, (err, filepath) => {
                if (!!err) {
                    console.log(JSON.stringify({ error: err }));
                }
                processed_que.push(...que.filter(q => q.path === iconData.Context).map(q => { q.icon = filepath; return q }));
                que = que.filter(q => q.path !== iconData.Context);

            });
    };

    var extract = (message) => {
        // console.log(JSON.stringify({ message: message }));
        if (message.path.indexOf('.exe') !== -1) {
            iconExtractor.getIcon(message.path, message.path);
        }
        if (message.path.indexOf('.lnk') !== -1) {
            ws.query(message.path, (err, data) => {
                if (err) {
                    console.log(JSON.stringify({ error: err }));
                }
                iconExtractor.getIcon(message.path, data.expanded.icon || data.icon || message.path);
            });
        }
    }
    iconExtractor.emitter.on('icon', _setResults);

    var que = [];
    var processed_que = [];

    // process.on('message', (message) => {

    que.push(...message)
    message.map(mes => extract(mes));
    var interval = setInterval(() => {
        if (que.length === 0) {
            callback.apply(this, [JSON.stringify(processed_que)]);
            processed_que = [];
            clearInterval(interval);
            return processed_que;
            // process.exit();
        }
    }, 100);
    // })
}
module.exports = start;