@echo off

REM runas /profile /user:administrator

IF EXIST "%AppData%\KeyLauncher" GOTO :THEEND

:NOWINDIR
mkdir %AppData%\KeyLauncher
mkdir %AppData%\KeyLauncher\icons
echo >%AppData%\KeyLauncher\settings.json
echo >%AppData%\KeyLauncher\index.json

:THEEND
echo End:: %AppData%\KeyLauncher
